# 15 - Configuration Management with Ansible - Dynamic Inventory

**Demo Project:**
Configure Dynamic Inventory

**Technologies used:**
Ansible, Terraform, AWS

**Project Description:**
- Create EC2 Instance with Terraform
- Write Ansible AWS EC2 Plugin to dynamically sets inventory of EC2 servers that Ansible should manage, instead of hard-coding server addresses in Ansible inventory file



15 - Configuration Management with Ansible
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp
